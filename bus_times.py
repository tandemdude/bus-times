import requests
from datetime import datetime, timezone
import dateutil.parser

bus_stop_api_link = 'https://api.tfl.gov.uk/StopPoint/490001143B/arrivals'

def times_of_next_buses():
	"""
	Returns times of the next buses
	"""
	stop_arrival_data = requests.get(bus_stop_api_link)
	return [dateutil.parser.parse(bus['expectedArrival']) for bus in stop_arrival_data.json()]

def get_next_buses():
	"""
	Return a list of the number of minutes to the next bus
	"""
	times = times_of_next_buses()
	minutes = []
	today = datetime.now(timezone.utc)
	for time in times:
		if time >= today:
			minutes.append(int((time - today).seconds/60))
	return sorted(minutes)


def main():
	print(get_next_buses())


if __name__ == '__main__':
	main()
